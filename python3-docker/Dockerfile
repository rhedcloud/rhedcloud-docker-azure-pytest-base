FROM launcher.gcr.io/google/ubuntu16_04

ENV CLOUDSDK_PYTHON="python3"

RUN apt-get -y update && \
    apt-get -y install gcc python3 python-dev python3-setuptools python3-pip wget ca-certificates \
       # These are necessary for add-apt-respository
       software-properties-common python-software-properties && \

    # Install Git >2.0.1
    add-apt-repository ppa:git-core/ppa && \
    apt-get -y update && \
    apt-get -y install git && \
  
    # Setup Google Cloud SDK (latest)
    mkdir -p /builder && \
    wget -qO- https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz | tar zxv -C /builder && \
    /builder/google-cloud-sdk/install.sh --usage-reporting=false \
        --bash-completion=false \
        --disable-installation-options && \
    # install crcmod: https://cloud.google.com/storage/docs/gsutil/addlhelp/CRC32CandInstallingcrcmod
    #easy_install -U pip3 && \
    pip3 install -U crcmod && \

    # Verify installation
    gcloud info \

    # Clean up
    apt-get -y remove gcc python3-dev python3-setuptools wget && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf ~/.config/gcloud

ENV PATH=/builder/google-cloud-sdk/bin/:$PATH

ENTRYPOINT ["/builder/google-cloud-sdk/bin/gcloud"]