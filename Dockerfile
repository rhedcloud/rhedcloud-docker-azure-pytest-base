FROM launcher.gcr.io/google/ubuntu16_04 AS base

# might switch to this base image?
#FROM mcr.microsoft.com/azure-powershell


MAINTAINER IT Arch <aws-itarch@mscloud.emory.net>

ARG bb_auth_string
ENV BB_AUTH_STRING=$bb_auth_string
ENV CLOUDSDK_PYTHON="python3"
ARG CLOUD_SDK_VERSION=276.0.0
ENV CLOUD_SDK_VERSION=$CLOUD_SDK_VERSION

RUN apt-get -y update && apt-get -y install \
        wget \
        gcc \
        python3 \
        python3-dev \
        python3-setuptools \
        python3-pip \
        python-pip \
        ca-certificates \
        zip \
        jq \
        git \
        moreutils \
        software-properties-common \
        python-software-properties

# upgrade pip because it's the right thing to do (it also makes it so we can
# use pip instead of remembering to use pip3)
# pip2 needed for network tools
RUN pip2 install -U pip && \
    pip3 install -U pip && \
    pip3 install -U crcmod

# Install network tools

RUN pip2 install -U ncclient
WORKDIR /opt/rhedcloud
RUN git clone https://jimmykincaid@bitbucket.org/jbkinca/emory-aws-vpn-csr1000v-lab.git

# Setup Microsoft Azure CLI
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

RUN az extension add --name blueprint

# install powershell
# Download the Microsoft repository GPG keys
RUN wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb

# Register the Microsoft repository GPG keys
RUN dpkg -i packages-microsoft-prod.deb

# Update the list of products
RUN apt-get update

# Install PowerShell
RUN apt-get install -y powershell
