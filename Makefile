IMG ?= emory/azure-pytest-base-1.0
BUILD_ARGS ?= --no-cache

build:
	docker build $(BUILD_ARGS) -t $(IMG) --build-arg bb_auth_string="$(BB_AUTH_STRING)" .

push: login
	docker push $(IMG):latest
ifdef BITBUCKET_BUILD_NUMBER
	docker tag $(IMG):latest $(IMG):$(BITBUCKET_BUILD_NUMBER)
	docker push $(IMG):$(BITBUCKET_BUILD_NUMBER)
endif

login:
	docker login --username $(DOCKER_HUB_USERNAME) --password $(DOCKER_HUB_PASSWORD)
